<html>

<?php
/*
•	Padarykite programą, kurioje turėtumėme vieną įvedimo lauką. Į šį lauką įvestumėte temperatūros nuoskaitas. Pvz.: 18, 16, 20, 21, 20, 18, 20, 24, 25, 21
•	Pasiverskite šį laukelį į masyvą (galite pasinaudoti f-ja explode). Jūsų sukurtas scenarijus turėtų išvesti:
Vidutinė temperatūra: 21,5 lapsniai
Viso nuoskaitų: 10
Dvi didžiausios temperatūros: 25, 24
Dvi mažiausios temperatūros: 16, 18
*/
?>

<head>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
<script src="https://code.jquery.com/jquery-3.1.1.slim.min.js" integrity="sha384-A7FZj7v+d/sdmMqp/nOQwliLvUsJfDHW+k9Omg/a/EheAdgtzNs3hpfag6Ed950n" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js" integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js" integrity="sha384-vBWWzlZJ8ea9aCX4pEW3rVHjgjt7zpkNpZk+02D9phzyeVkE+jo0ieGizqPLForn" crossorigin="anonymous"></script>
</head>

<body>   

<br>
<h1 align="center">6 užduotis</h1>
<br>

<form method="GET">
<div class="form-group row">
  <label for="example-text-input" class="col-2 col-form-label" align="center">Įveskite temperatūros nuoskaitas:</label>
  <div class="col-6">
    <input class="form-control" type="text" name="temperature" value=" " id="example-text-input">    
  </div>
  <button type="submit" class="btn btn-primary">Patvirtinti</button>
</div>

<?php

if (!empty($_GET['temperature'])) {
    $temperatureArray = explode(', ', $_GET['temperature']);
    $countTemp = count($temperatureArray);
    $averageTemperature = array_sum($temperatureArray) / $countTemp;
    echo 'Viso nuoskaitų: ' . $countTemp . '<br>'; 
    echo 'Vidutinė temperatūra: ' . $averageTemperature . '<br>';

    $sortedArray = sort($temperatureArray);
    echo 'Dvi didžiausios temperatūros: ' . end($temperatureArray) . ', ' . $temperatureArray[$countTemp-2] . '<br>';
    echo 'Dvi mažiausios temperatūros: ' . reset($temperatureArray) . ', ' . $temperatureArray[1] . '<br>';
}
?>
</form>

</body>

</html>