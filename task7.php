<?php
/*
Duotas mėnesinių atlyginimų masyvas.
•	3 Lentelės stulpeliai: Metinis atlyginimas, Mėnesinis atlyginimas, Valandinis atlyginimas
•	Panaudoti foreach ciklą sugeneruoti atlyginimų lentelei. 
•	Valandinis atlyginimas skaičiuojamas pagal formulę: mėnesinis atlyginimas / 168.
•	Metinis atlyginimas skaičiuojamas pagal formulę: mėnesinis atlyginimas * 12
*/

$salaries = [400, 500, 600, 700, 800, 1000, 1200, 1500, 1800, 2000, 2500, 3000, 4000, 5000, 8000, 10000];
?>

<h1 align="center">7 užduotis</h1>

<table border="1">
    <tr>
        <th>Metinis atlyginimas</th>
        <th>Mėnesinis atlyginimas</th>
        <th>Valandinis atlyginimas</th>
    </tr>

    <?php foreach ($salaries as $key => $salary) {
        $hourlySalary = round($salary / 168, 2);
        $annualSalary = $salary * 12;?>
    <tr>
        <td><?php echo $annualSalary; ?></td>
        <td><?php echo $salary; ?></td>
        <td><?php echo $hourlySalary; ?></td>
    </tr>
    <?php }?>
</table>
